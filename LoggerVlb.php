<?php
/*
 * -----------------------------------------------------------------------------
 *  Project             : vlb.api.account    
 *  Date Creation       : Apr 2, 2018 
 *  Filename            : LoggerVlb.php
 *  Author              : IZIordanov
 * -----------------------------------------------------------------------------
 *  Copyright(C) 2000-2018 IZIordanov
 *  
 *  This program is free software; you can redistribute it and/or modify it under 
 *  the terms of the GNU General Public License published by the Free Software 
 *  Foundation.
 * -----------------------------------------------------------------------------
 */

global $loggerVlb;

require_once("LoggerBase.php");
Logger::configure(dirname(__FILE__).'/appender_pdo.properties');

/**
 * Description of LoggerVlb
 *
 * @author IZIordanov
 */
class LoggerVlb extends LoggerBase{
     
    // <editor-fold defaultstate="collapsed" desc="__construct">
    public function __construct() {
        parent::__construct();
        $this->MN = "LoggerVlb: ";
        try {
            
            
            $this->logger = Logger::getRootLogger();
           
            $this->logger->trace("LoggerVlb __construct");
           
        } catch (Exception $ex) {
            echo "LoggerVlb Error: " . $ex."<br/>";
        }
    }
    
    // </editor-fold>
    
    
    // <editor-fold defaultstate="collapsed" desc="Methods">
    
    public static function currLogger(){
         global $loggerVlb;
        if(!isset($loggerVlb))
        {
            $loggerVlb = new LoggerVlb();
        }
        
        return $loggerVlb;
    }
    
    public static function logBegin($mn)
    {
        LoggerVlb::currLogger()->begin($mn);
    }
    
     public static function logEnd($mn)
    {
        LoggerVlb::currLogger()->end($mn);
    }
    
     public static function log($mn, $msg)
    {
        LoggerVlb::currLogger()->debug($mn,$msg);
    }
    
    public static function logError($mn, $ex)
    {
        LoggerVlb::currLogger()->error($mn);
    }
    // </editor-fold>
}
