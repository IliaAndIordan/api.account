<?php

/*
 * -----------------------------------------------------------------------------
 *  Project             : vlb.api.account    
 *  Date Creation       : Apr 2, 2018 
 *  Filename            : ConnectionVlb.php
 *  Author              : IZIordanov
 * -----------------------------------------------------------------------------
 *  Copyright(C) 2000-2018 IZIordanov
 *  
 *  This program is free software; you can redistribute it and/or modify it under 
 *  the terms of the GNU General Public License published by the Free Software 
 *  Foundation.
 * -----------------------------------------------------------------------------
 */

global $vlbDbConnection;
global $contentPage;

require_once("config.inc.php");
require_once("ConnectionBase.class.php");

/**
 * Description of ConnectionVlb
 *
 * @author IZIordanov
 */
class ConnectionVlb extends ConnectionBase {
    private $dbHost=null;
    private $dbName=null;
    private $dbUser=null;
    private $dbPassword=null;
    private $dbPort=null;
    
    //establish db connection
    public function __construct() {
        $mn = "ConnectionVlb:__construct()";
        LoggerVlb::logBegin($mn);
        parent::__construct(DB_HOST, DB_USER, DB_PASS, DB_NAME, DB_PORT);
        
        $this->dbHost = DB_HOST;
        $this->dbUser = DB_USER;
        $this->dbPassword = DB_PASS;
        $this->dbName = DB_NAME;
        $this->dbPort = DB_PORT;
        try {
            $this->connection->query("SET NAMES 'utf8' COLLATE 'utf8_unicode_ci'");


            // Will not affect $mysqli->real_escape_string();
            $this->connection->query("SET CHARACTER SET utf8");

            // But, this will affect $mysqli->real_escape_string();
            $this->connection->set_charset('utf8');

            $charset = $this->connection->character_set_name();
            
            LoggerVlb::log($mn, "Connection to ".DB_NAME." established. Charset:".$charset);
            if (mysqli_connect_errno()) {
                LoggerVlb::log("$mn", "Database connect Error : " . mysqli_connect_error($this->connection));
                //header('Location: /dberror.html');
                //die();
                //header("Location: ".$url);
                //ob_flush();
            }
        } catch (Exception $ex) {
            echo 'Exception:' . $ex;
            LoggerVlb::logError($mn, $ex);
        }
        LoggerVlb::logEnd($mn);
    }
    
    public static function dbConnect() {
        global $vlbDbConnection;
        if(!isset($vlbDbConnection))
        {
            $vlbDbConnection = new ConnectionVlb();
        }
        
        return $vlbDbConnection;
        
    }
}
