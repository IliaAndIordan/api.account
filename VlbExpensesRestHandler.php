<?php

/*
 * -----------------------------------------------------------------------------
 *  Project             : api.account    
 *  Date Creation       : Apr 6, 2018 
 *  Filename            : VlbExpensesRestHandler.php
 *  Author              : IZIordanov
 * -----------------------------------------------------------------------------
 *  Copyright(C) 2000-2018 IZIordanov
 *  
 *  This program is free software; you can redistribute it and/or modify it under 
 *  the terms of the GNU General Public License published by the Free Software Foundation.
 * -----------------------------------------------------------------------------
 */
require_once("LoggerVlb.php");
require_once("./ConnectionVlb.php");
require_once("SimpleRest.class.php");
require_once 'Response.class.php';
require_once("JwtAuth.php");
require_once("VlbUser.class.php");
require_once("VlbVehicle.class.php");

/**
 * Description of VlbVehicleRestHandler
 *
 * @author IZIordanov
 */
class VlbExpensesRestHandler extends SimpleRest {
    
    // <editor-fold defaultstate="collapsed" desc=" Common Methods">
    
    public static function getDate($data) {
        $retValue = null;
        if (isset($data) && $data != null) {
            if (!is_object($data)) {
                $retValue = new DateTime($data);
            } else {
                $retValue = $this->purchase_date;
            }
        }
        return $retValue;
    }
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc=" Expences Methods">
  
    public static function VehicleExpenses($id, $userId) {
        $mn = "VlbExpensesRestHandler::VehicleExpenses('.$id.')";
        LoggerVlb::logBegin($mn);
        $response = null;
        try {
            $vlbVehicle = new VlbVehicle();
            $vlbVehicle->loadById($id);
            if (isset($vlbVehicle)) {
                    $response = new Response("success", " Expences loaded.");
                $response->addData("vehicle",$vlbVehicle);
                $response->addData("userId",$userId);
                
                // Stat
                $sql = "SELECT e.id as exp_id, e.vehicle_id, e.exp_date, e.exp_millage, e.exp_price, v.currency_lbl,  e.exp_note, e.track_id 
                        FROM iordanov_vlb.vlb_expenses e
                        join iordanov_vlb.vlb_vehicle v on v.vehicle_id = e.vehicle_id
                        where e.vehicle_id=?
                        order by e.exp_millage desc, e.exp_date desc";
                
                $bound_params_r = array('i', $id);
                $conn = ConnectionVlb::dbConnect();
                $logModel = LoggerVlb::currLogger()->getModule($mn);
                $ret_stat = $conn->SelectJson($sql, $bound_params_r, $logModel);
            
                $response->addData("vehicle_expences",$ret_stat);
                
           
            }
            else{
               $response = new Response("error", " vehicle expences list NOT found.");
            }
        } catch (Exception $ex) {
            LoggerVlb::logError($mn,  $ex);
            $response = new Response($ex);
        }
        LoggerVlb::log($mn, " response = " . $response->toJSON());
        LoggerVlb::logEnd($mn);
        $rh = new VlbVehicleRestHandler();
        $rh->EncodeResponce($response);
    }
    /**
     *  
     * @param type $id
     * @param type $expense
     * @param type $userId
     */
    public static function ExpenseSave($expense, $userId) {
        $mn = "VlbExpensesRestHandler::ExpenseSave()";
        LoggerVlb::logBegin($mn);
        $response = null;
        try {
            
            if (isset($expense) && $expense->exp_id > 0 ) {
                $sql = "INSERT INTO iordanov_vlb.vlb_expenses
                    (vehicle_id, exp_date, exp_millage, exp_price, exp_note, track_id)
                    VALUES (?, ?, ?, ?, ?, ?) ";
                
                $sql ="UPDATE iordanov_vlb.vlb_expenses
                    SET  vehicle_id = ?,
                    exp_date = ?,
                    exp_millage = ?,
                    exp_price = ?,
                    exp_note = ?
                    WHERE id = ? ";
                $dv =  DateTime::createFromFormat('Y-m-d H:i:s',$expense->exp_date);
                $bound_params_r = array('isidsi',
                    $expense->vehicle_id,
                    ($dv!=null ? $dv->format("Y-m-d H:i:s") : null),
                    $expense->exp_millage,
                    $expense->exp_price,
                    $expense->exp_note,
                    $expense->exp_id);
                $conn = ConnectionVlb::dbConnect();
                $logModel = LoggerVlb::currLogger()->getModule($mn);
                $affectedRows = $conn->preparedUpdate($sql, $bound_params_r, $logModel);
                LoggerVlb::log($mn, "affectedRows=" . $affectedRows);
            }
            else{
               $response = new Response("error", " expense NOT found.");
            }
        } catch (Exception $ex) {
            LoggerVlb::logError($mn,  $ex);
            $response = new Response($ex);
        }
        if(isset($response)){
            LoggerVlb::log($mn, " response = " . $response->toJSON());
            LoggerVlb::logEnd($mn);
            $rh = new VlbVehicleRestHandler();
            $rh->EncodeResponce($response);
        }
        else
            VlbExpensesRestHandler::VehicleExpenses($expense->vehicle_id, $userId);
    }
    
    public static function ExpenseAdd($expense, $userId) {
        $mn = "VlbExpensesRestHandler::ExpenseAdd()";
        LoggerVlb::logBegin($mn);
        $response = null;
        try {
            
            if (isset($expense) ) {
                $sql = "INSERT INTO iordanov_vlb.vlb_expenses
                    (vehicle_id, exp_date, exp_millage, exp_price, exp_note, track_id)
                    VALUES (?, ?, ?, ?, ?, ?) ";
                
                $dv =  DateTime::createFromFormat('Y-m-d H:i:s',$expense->exp_date);
                $bound_params_r = array('isidsi',
                    $expense->vehicle_id,
                    ($dv!=null ? $dv->format("Y-m-d H:i:s") : null),
                    $expense->exp_millage,
                    floatval($expense->exp_price),
                    $expense->exp_note,
                    $expense->track_id);
                $conn = ConnectionVlb::dbConnect();
                $logModel = LoggerVlb::currLogger()->getModule($mn);
                $id = $conn->preparedInsert($sql, $bound_params_r, $logModel);
                LoggerVlb::log($mn, "inserted expense id=" . $id);
            }
            else{
               $response = new Response("error", " expense NOT found.");
            }
        } catch (Exception $ex) {
            LoggerVlb::logError($mn,  $ex);
            $response = new Response($ex);
        }
        if(isset($response)){
            LoggerVlb::log($mn, " response = " . $response->toJSON());
            LoggerVlb::logEnd($mn);
            $rh = new VlbVehicleRestHandler();
            $rh->EncodeResponce($response);
        }
        else
            VlbExpensesRestHandler::VehicleExpenses($expense->vehicle_id, $userId);
    }
    
    
    
    public static function VehicleNew($vehicle, $userId) {
        $mn = "VlbVehicleRestHandler::VehicleNew('.$userId.')";
        LoggerVlb::logBegin($mn);
        $response = null;
        try {
            $vlbVehicle = VlbVehicle::Create($userId, $vehicle->model, $vehicle->milage_km_start);
            $vlbVehicle->loadById($id);
            if (isset($vlbVehicle) && $vlbVehicle->getUserId() == $userId) {
                
                $vlbVehicle->setName($vehicle->name);
                $vlbVehicle->setModel($vehicle->model);
                //LoggerVlb::log($mn, " purchase_date = " . $vehicle->purchase_date->date);
                $vlbVehicle->setPurchaseDate($vehicle->purchase_date->date);
                $vlbVehicle->setFirstRegistration($vehicle->first_reg->date);
                if(isset($vehicle->sale_date) && isset($vehicle->sale_date->date))
                    $vlbVehicle->setSaleDate($vehicle->sale_date->date);
                $vlbVehicle->setMilageKmStart($vehicle->milage_km_start);
                $vlbVehicle->setMilageKmCurrent($vehicle->milage_km_current);
                $vlbVehicle->setTrackInMi($vehicle->track_in_mi);
                
                $updatedVehicle = $vlbVehicle->save();
                
                $response = new Response("success", " vehicles created.");
                $response->addData("vehicle",$updatedVehicle);
                $response->addData("userId",$userId);
            }
            else{
               $response = new Response("error", " vehicles NOT found.");
            }
        } catch (Exception $ex) {
            LoggerVlb::logError($mn,  $ex);
            $response = new Response($ex);
        }
        LoggerVlb::log($mn, " response = " . $response->toJSON());
        LoggerVlb::logEnd($mn);
        $rh = new VlbVehicleRestHandler();
        $rh->EncodeResponce($response);
    }
    
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc=" Gas Methods">
  
    public static function VehicleGasLoad($id, $userId) {
        $mn = "VlbExpensesRestHandler::VehicleGasLoad('.$id.')";
        LoggerVlb::logBegin($mn);
        $response = null;
        try {
            if (isset($id)) {
                    $response = new Response("success", " data loaded.");
                $response->addData("vehicle_id",$id);
                $response->addData("userId",$userId);
                
                // Stat
                $sql = "SELECT g.id as gas_id, g.vehicle_id, g.gas_date, g.gas_millage, g.gas_quantity, 
                g.gas_total_price, v.currency_lbl, g.gas_location_id, g.cal_price_per_l, 
                g.cal_fule_consumption, g.brand as location_brand, st.fule_economy, st.avg_gas_price
                FROM iordanov_vlb.vlb_gas g
                join iordanov_vlb.vlb_vehicle v on v.vehicle_id = g.vehicle_id
                join iordanov_vlb.vlb_vehicle_stat st on st.vehicle_id = g.vehicle_id
                where g.vehicle_id=?
                order by gas_millage desc, g.gas_date desc";
                
                $bound_params_r = array('i', $id);
                $conn = ConnectionVlb::dbConnect();
                $logModel = LoggerVlb::currLogger()->getModule($mn);
                $ret_stat = $conn->SelectJson($sql, $bound_params_r, $logModel);
            
                $response->addData("vehicle_gas_all",$ret_stat);
                
           
            }
            else{
               $response = new Response("error", " no vehicle id provided.");
            }
        } catch (Exception $ex) {
            LoggerVlb::logError($mn,  $ex);
            $response = new Response($ex);
        }
        LoggerVlb::log($mn, " response = " . $response->toJSON());
        LoggerVlb::logEnd($mn);
        $rh = new VlbVehicleRestHandler();
        $rh->EncodeResponce($response);
    }
    /**
     *  GasSave
     * @param type $gas
     * @param type $userId
     */
    public static function GasSave($gas, $userId) {
        $mn = "VlbExpensesRestHandler::GasSave()";
        LoggerVlb::logBegin($mn);
        $response = null;
        try {
            
            if (isset($gas) && $gas->gas_id > 0 ) {
                
                $sql ="UPDATE iordanov_vlb.vlb_gas
                    SET  vehicle_id = ?,
                    gas_date = ?,
                    gas_millage = ?,
                    gas_quantity = ?,
                    gas_total_price = ?,
                    gas_location_id=?,
                    cal_price_per_l=?,
                    cal_fule_consumption=?,
                    brand=?
                    WHERE id = ? ";
                
                LoggerVlb::log($mn, " gas_date = " .  $gas->gas_date);  
                $dv =  DateTime::createFromFormat('Y-m-d H:i:s',$gas->gas_date);
                if($dv==null){
                    $dv = DateTime::createFromFormat("Y-m-d\TH:i:s.uP", $gas->gas_date);
                }
                if($dv==null){
                    LoggerVlb::log($mn, " No dv, gas->gas_date=" . $gas->gas_date);
                }
                else{
                    LoggerVlb::log($mn, " dv = " . $dv->format("Y-m-d H:i:s"));  
                }
                $cal_price_per_l = $gas->gas_quantity>0?($gas->gas_total_price/$gas->gas_quantity):0;
                $bound_params_r = array('isiddiddsi',
                    $gas->vehicle_id,
                    ($dv!=null ? $dv->format("Y-m-d H:i:s") : null),
                    $gas->gas_millage,
                    $gas->gas_quantity,
                    $gas->gas_total_price,
                    $gas->gas_location_id,
                    $cal_price_per_l,
                    $gas->cal_fule_consumption,
                    $gas->location_brand,
                    $gas->gas_id);
                $conn = ConnectionVlb::dbConnect();
                $logModel = LoggerVlb::currLogger()->getModule($mn);
                $affectedRows = $conn->preparedUpdate($sql, $bound_params_r, $logModel);
                LoggerVlb::log($mn, "affectedRows=" . $affectedRows);
            }
            else{
               $response = new Response("error", " gas NOT found.");
            }
        } catch (Exception $ex) {
            LoggerVlb::logError($mn,  $ex);
            $response = new Response($ex);
        }
        if(isset($response)){
            LoggerVlb::log($mn, " response = " . $response->toJSON());
            LoggerVlb::logEnd($mn);
            $rh = new VlbVehicleRestHandler();
            $rh->EncodeResponce($response);
        }
        else{
            VlbExpensesRestHandler::VehicleGasLoad($gas->vehicle_id, $userId);
        }
    }
    
    public static function GasAdd($gas, $userId) {
        $mn = "VlbExpensesRestHandler::GasAdd()";
        LoggerVlb::logBegin($mn);
        $response = null;
        try {
            
            if (isset($gas) ) {
                $sql = "INSERT INTO iordanov_vlb.vlb_gas
                    (vehicle_id, gas_date, gas_millage, gas_quantity, gas_total_price, gas_location_id, cal_price_per_l, cal_fule_consumption, brand)
                    VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?) ";
                
                LoggerVlb::log($mn, " gas_date = " .  $gas->gas_date);  
                $dv =  DateTime::createFromFormat('Y-m-d H:i:s',$gas->gas_date);
                if($dv==null){
                    $dv = DateTime::createFromFormat("Y-m-d\TH:i:s.uP", $gas->gas_date);
                }
                if($dv==null){
                    LoggerVlb::log($mn, " No dv, gas->gas_date=" . $gas->gas_date);
                }
                else{
                    LoggerVlb::log($mn, " dv = " . $dv->format("Y-m-d H:i:s"));  
                }
                $cal_price_per_l = $gas->gas_quantity>0?($gas->gas_total_price/$gas->gas_quantity):0;
                $bound_params_r = array('isiddidds',
                    $gas->vehicle_id,
                    ($dv!=null ? $dv->format("Y-m-d H:i:s") : null),
                    $gas->gas_millage,
                    $gas->gas_quantity,
                    $gas->gas_total_price,
                    $gas->gas_location_id,
                    $gas->cal_price_per_l,
                    $gas->cal_fule_consumption,
                    $gas->location_brand);
                $conn = ConnectionVlb::dbConnect();
                $logModel = LoggerVlb::currLogger()->getModule($mn);
                $id = $conn->preparedInsert($sql, $bound_params_r, $logModel);
                LoggerVlb::log($mn, "inserted gas id=" . $id);
            }
            else{
               $response = new Response("error", " gas NOT found.");
            }
        } catch (Exception $ex) {
            LoggerVlb::logError($mn,  $ex);
            $response = new Response($ex);
        }
        if(isset($response)){
            LoggerVlb::log($mn, " response = " . $response->toJSON());
            LoggerVlb::logEnd($mn);
            $rh = new VlbVehicleRestHandler();
            $rh->EncodeResponce($response);
        }
        else
            VlbExpensesRestHandler::VehicleGasLoad($gas->vehicle_id, $userId);
    }
    
    
    // </editor-fold>
}
