<?php

/*
 * -----------------------------------------------------------------------------
 *  Project             : api.account    
 *  Date Creation  : Apr 6, 2018 
 *  Filename           : VlbAccountRestHandler.php
 *  Author              : IZIordanov
 * -----------------------------------------------------------------------------
 *  Copyright(C) 2000-2018 IZIordanov
 *  
 *  This program is free software; you can redistribute it and/or modify it under 
 *  the terms of the GNU General Public License published by the Free Software Foundation.
 * -----------------------------------------------------------------------------
 */
require_once("LoggerVlb.php");
require_once("./ConnectionVlb.php");
require_once("SimpleRest.class.php");
require_once 'Response.class.php';
require_once 'SimpleRest.class.php';
require_once("JwtAuth.php");
require_once("VlbUser.class.php");
/**
 * Description of VlbAccountRestHandler
 *
 * @author IZIordanov
 */
class VlbAccountRestHandler extends SimpleRest {
    
    // <editor-fold defaultstate="collapsed" desc="No JWT Tocken Methods">
    
    public function Option() {
        $mn = "VlbAccountRestHandler::Option()";
        $response = new Response("success", "Service working.");
        
        $rh = new VlbAccountRestHandler();
        $rh->EncodeResponce($response);
    }
    
    public function Ping() {
        $mn = "VlbAccountRestHandler::Ping()";
        LoggerVlb::logBegin($mn);
        $response = null;
        try {
            $conn = ConnectionVlb::dbConnect();
            if (isset($conn)) {
                LoggerVlb::log($mn, " response = " . "Service working");
                $response = new Response("success", "Service working.");
            }
            else
            {
                $response = new Response("success", "There is something wrong but generati I am alive.");
            }
            
        } catch (Exception $ex) {
            LoggerVlb::logError($mn,  $ex);
            $response = new Response($ex);
        }
        LoggerVlb::log($mn, " response = " . $response->toJSON());
        LoggerVlb::logEnd($mn);

        $this->EncodeResponce($response);
    }
    
    public function Login($email, $password) {
        $mn = "VlbAccountRestHandler::Login('.$email.', '.$password.')";
        LoggerVlb::logBegin($mn);
        $response = null;
        try {
            $vlbUser = VlbUser::login($email, $password);
            if (isset($vlbUser)) {
                LoggerVlb::log($mn, " user = " . $vlbUser->toJson());
                if(JwtAuth::signTocken($vlbUser->getId(), $vlbUser->getName())){
                    $response = new Response("success", "JwtAuth Set.");
                    $currUser = new User($vlbUser);
                    
                    $response->addData("current_user",$currUser);
                }
                else{
                   $response = new Response("error", "JwtAuth NOT Set.");
                   $response->statusCode=401;
                }
            }
            else
            {
                $response = new Response("error", "Invalid Credentioals.");
                $response->statusCode=401;
            }
            
        } catch (Exception $ex) {
            LoggerVlb::logError($mn,  $ex);
            $response = new Response($ex);
        }
        LoggerVlb::log($mn, " response = " . $response->toJSON());
        LoggerVlb::logEnd($mn);

        $this->EncodeResponce($response);
    }
    
    public function refreshToken() {
        $mn = "VlbAccountRestHandler::refreshToken()";
        LoggerVlb::logBegin($mn);
        $response = null;
        try {
            $payload = JwtAuth::Autenticate();
            if(isset($payload))
            {
                $vlbUser = new VlbUser();
                $vlbUser->loadById(intval($payload->data->user_id));
                //LoggerVlb::log($mn, " user = " . $vlbUser->toJSON());
                //--- Validate User Access
                if($vlbUser->getRole()>0)
                {
                    $success = JwtAuth::RefreshTocken();
                    
                    $response = new Response("success", "JwtAuth RefreshTocken.");
                    $currUser = new User($vlbUser);
                    
                    $response->addData("current_user",$currUser);
                    $response->statusCode=200;
                }
            }
            else
            {
                $response = new Response("error", "Invalid Credentioals.");
                $response->statusCode=200;
            }
            
        } catch (Exception $ex) {
            LoggerVlb::logError($mn,  $ex);
            $response = new Response($ex);
        }
        //LoggerVlb::log($mn, " response = " . $response->toJSON());
        LoggerVlb::logEnd($mn);

        $this->EncodeResponce($response);
    }
    
     // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="Multilanguage Support">
    
    public static function MlInsert($lngObj) {
        $mn = "VlbAccountRestHandler::MlLoad('.$lngObj->key.')";
        LoggerVlb::logBegin($mn);
        $response = null;
        try {
            
            if (isset($lngObj->key)) {
                // Stat
                $sql = "call ml_insert(?, ?);";
                
                $bound_params_r = array('ss', $lngObj->key, 'en');
                $conn = ConnectionVlb::dbConnect();
                $logModel = LoggerVlb::currLogger()->getModule($mn);
                $affectedRows = $conn->preparedInsert($sql, $bound_params_r, $logModel);
                $msg = "Translation for " . $lngObj->key . " inserted."." Affected Rows: " . $affectedRows;
                LoggerVlb::log($mn, $msg);
                $response = new Response("success", $msg);
            }
            else{
               $response = new Response("error", " Translation NOT inserted.");
            }
        } catch (Exception $ex) {
            LoggerVlb::logError($mn,  $ex);
            $response = new Response($ex);
        }
        LoggerVlb::log($mn, " response = " . $response->toJSON());
        LoggerVlb::logEnd($mn);
        $rh = new VlbAccountRestHandler();
        $rh->EncodeResponce($response);
    }
    
    public static function MlLoad($lngObj) {
        $mn = "VlbAccountRestHandler::MlLoad('.$lngObj->key.')";
        LoggerVlb::logBegin($mn);
        $language = "en";
        $response = null;
        try {
            
            if (isset($lngObj->language)) {
                $language = $lngObj->language;
            }
            // Stat
            $sql = "SELECT lbl_key as 'KEY', lbl_value as 'value' FROM iordanov_vlb.vlb_label
                    where lbl_language=?
                    order by lbl_key";

            $bound_params_r = array('s', $language);
            $conn = ConnectionVlb::dbConnect();
            $logModel = LoggerVlb::currLogger()->getModule($mn);
            $ret_stat = $conn->SelectJson($sql, $bound_params_r, $logModel);
            $response = new Response("success", "Translation language: " . $language . ", keys: ". sizeof($ret_stat).".");
            $response->addData("labels",$ret_stat);
            $response->addData("language",$language);
            
        } catch (Exception $ex) {
            LoggerVlb::logError($mn,  $ex);
            $response = new Response($ex);
        }
        LoggerVlb::log($mn, " response = " . $response->toJSON());
        LoggerVlb::logEnd($mn);
        $rh = new VlbAccountRestHandler();
        $rh->EncodeResponce($response);
    }
    // </editor-fold>
    
}
