<?php

/*
 * -----------------------------------------------------------------------------
 *  Project             : vlb.api.account    
 *  Date Creation       : Apr 2, 2018 
 *  Filename            : VlbAccountController.php
 *  Author              : IZIordanov
 * -----------------------------------------------------------------------------
 *  Copyright(C) 2000-2018 IZIordanov
 *  
 *  This program is free software; you can redistribute it and/or modify it under 
 *  the terms of the GNU General Public License published by the Free Software Foundation.
 * -----------------------------------------------------------------------------
 * This is a Controller file that receives the request and dispatches it to 
 * respective hendler for processing. 
 * ‘view’ key is used to identify the URL request.
 * -----------------------------------------------------------------------------
 */

date_default_timezone_set('Europe/Helsinki');
//mb_internal_encoding("UTF-8"); 
if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
//This is a server using Windows
    $delim = ";";
    $slash = "\\";
} else {
//This is a server not using Windows!
    $delim = ":";
    $slash = "/";
}

define('APP_HOME', dirname(dirname((__FILE__))));
define('SLASH', $slash);

ini_set("include_path", ini_get("include_path") . $delim . '/home/iordanov/php');

ini_set('include_path', ini_get('include_path') .
        $delim . '/home/iordanov/common/lib' . $delim . '/home/iordanov/common/lib/iiordan' .
        $delim . '/home/iordanov/common/lib/vlb' .
        $delim . '/home/iordanov/common/lib/log4php' .
        $delim . '/home/iordanov/common//lib/log4php/configurators');



$domain = ($_SERVER['HTTP_HOST'] != 'localhost') ? $_SERVER['HTTP_HOST'] : false;
//setcookie('cookiename', 'vlb.iordanov.info', time() + 60 * 60 * 24 * 365, '/', $domain, false);
//display_errors = On
ini_set("display_errors", "1");

ob_start();

$mn = "VlbAccountController.php";
//--- Include CORS
require_once("rest_cors_header.php");

require_once("LoggerVlb.php");
require_once("Functions.php");
require_once("./VlbAccountRestHandler.php");
LoggerVlb::logBegin($mn);

$view = "";

if (isset($_REQUEST["view"]))
    $view = $_REQUEST["view"];


LoggerVlb::log($mn, "view=" . $view . ", id=" . $id);

if ($_SERVER['REQUEST_METHOD'] == "OPTIONS") {
    $restHendler = new VlbAccountRestHandler();
    $restHendler->Option();
    LoggerVlb::logEnd($mn);
} else {

    // get the HTTP method, path and body of the request
    $method = $_SERVER['REQUEST_METHOD'];
    /*
      controls the RESTful services URL mapping
     */
    switch ($view) {

        case "ping":
            // to handle REST Url /pcpd/
            $restHendler = new VlbAccountRestHandler();
            $restHendler->Ping($id);
            break;
        case "mlload":
            $dataPayload = json_decode(file_get_contents('php://input'));
            VlbAccountRestHandler::MlLoad($dataPayload);
            break;
        case "ml":
            $dataPayload = json_decode(file_get_contents('php://input'));
            switch ($method) {
                case 'PUT':
                    VlbAccountRestHandler::MlUpdate($dataPayload);
                    break;
                case 'POST':
                    VlbAccountRestHandler::MlInsert($dataPayload);
                    break;
                case 'DELETE':
                    VlbAccountRestHandler::MlDelete($dataPayload);
                    break;
            }
            break;
        case "login":
            // to handle REST Url /pcpd/
            $restHendler = new VlbAccountRestHandler();
            // read JSon input
            $dataJson = json_decode(file_get_contents('php://input'));
            if (isset($dataJson))
                $restHendler->Login($dataJson->username, $dataJson->password);

            break;
        case "refreshtoken":
            // to handle REST Url /pcpd/

            $restHendler = new VlbAccountRestHandler();
            $restHendler->refreshToken();

            break;
        default:
            LoggerVlb::log($mn, "No heandler for view: " . $view);
            break;
    }
}


LoggerVlb::logEnd($mn);

