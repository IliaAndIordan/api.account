<?php

/*
 * -----------------------------------------------------------------------------
 *  Project             : vlb.api.account    
 *  Date Creation       : Apr 2, 2018 
 *  Filename            : VlbVehicleController.php
 *  Author              : IZIordanov
 * -----------------------------------------------------------------------------
 *  Copyright(C) 2000-2018 IZIordanov
 *  
 *  This program is free software; you can redistribute it and/or modify it under 
 *  the terms of the GNU General Public License published by the Free Software Foundation.
 * -----------------------------------------------------------------------------
 * This is a Controller file that receives the request and dispatches it to 
 * respective hendler for processing. 
 * ‘view’ key is used to identify the URL request.
 * -----------------------------------------------------------------------------
 */

date_default_timezone_set('Europe/Helsinki');
//mb_internal_encoding("UTF-8"); 
if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
//This is a server using Windows
    $delim = ";";
    $slash = "\\";
} else {
//This is a server not using Windows!
    $delim = ":";
    $slash = "/";
}

define('APP_HOME', dirname(dirname((__FILE__))));
define('SLASH', $slash);

ini_set("include_path", ini_get("include_path") . $delim . '/home/iordanov/php');

ini_set('include_path', ini_get('include_path') .
        $delim . '/home/iordanov/common/lib' . $delim . '/home/iordanov/common/lib/iiordan' .
        $delim . '/home/iordanov/common/lib/vlb' .
        $delim . '/home/iordanov/common/lib/log4php' .
        $delim . '/home/iordanov/common//lib/log4php/configurators');



$domain = ($_SERVER['HTTP_HOST'] != 'localhost') ? $_SERVER['HTTP_HOST'] : false;
//setcookie('cookiename', 'vlb.iordanov.info', time() + 60 * 60 * 24 * 365, '/', $domain, false);
//display_errors = On
ini_set("display_errors", "1");

ob_start();

$mn = "VlbVehicleController.php";
//--- Include CORS
require_once("rest_cors_header.php");

//--- Libraries
require_once("LoggerVlb.php");
require_once("Functions.php");
require_once("JwtAuth.php");
require_once("./VlbAccountRestHandler.php");
require_once("./VlbVehicleRestHandler.php");
require_once("./VlbExpensesRestHandler.php");

LoggerVlb::logBegin($mn);

// returns AutenticateResult{payload, isValud, message}
$payload = null;
$authRes = JwtAuth::Autenticate();
if (isset($authRes)) {
    if ($authRes->isValud) {
        $payload = $authRes->payload;
    } else {
        $response = new Response("error", $authRes->message);
        $response->statusCode = 401;
        $rh = new VlbVehicleRestHandler();
        $rh->EncodeResponce($response);
        return;
    }
}

$method = $_SERVER['REQUEST_METHOD'];
if (isset($payload) || ($payload->data->user_id > 0)) {
    $userId = $payload->data->user_id;
    $userName = $payload->data->user_name;
    $view = "";
    $dataPayload = null;

    if (isset($_REQUEST["view"]))
        $view = $_REQUEST["view"];
    // post request data payload
    $dataPayload = json_decode(file_get_contents('php://input'));

    LoggerVlb::log($mn, "view: " . $view . ", userId=" . $userId . ", has dataPayload: " . isset($dataPayload));

    /*
      controls the RESTful services URL mapping
     */
    switch ($view) {

        case "vehicles":
            VlbVehicleRestHandler::Vehicles($userId, $userName);
            break;
        case "vehicle":
            if (isset($dataPayload))
                VlbVehicleRestHandler::Vehicle($dataPayload->vehicleId, $userId);
            break;
        case "expenses":
            if (isset($dataPayload))
                VlbExpensesRestHandler::VehicleExpenses($dataPayload->vehicleId, $userId);
            break;
        case "expense":
            if (isset($dataPayload)) {
                switch ($method) {
                    case 'PUT':
                        VlbExpensesRestHandler::ExpenseAdd($dataPayload->expense, $userId);
                        break;
                    case 'POST':
                        VlbExpensesRestHandler::ExpenseSave($dataPayload->expense, $userId);
                        break;
                    case 'DELETE':
                        $response = new Response("error", "No heandler for view: " . $view);
                        $response->statusCode = 401;
                        $rh = new VlbVehicleRestHandler();
                        $rh->EncodeResponce($response);
                        break;
                }
            }

            break;

        case "vehicleedit":

            switch ($method) {
                case 'PUT':
                    VlbVehicleRestHandler::VehicleNew($dataPayload->vehicle, $userId);
                    break;
                case 'POST':
                    VlbVehicleRestHandler::VehicleSave($dataPayload->vehicle->vehicleId, $dataPayload->vehicle, $userId);
                    break;
                case 'DELETE':
                    if (isset($_REQUEST["id"])) {
                        $id = $_REQUEST["id"];
                        LoggerVlb::log($mn, "DELETE for id: " . $id);
                        VlbVehicleRestHandler::VehicleDelete($id, $userId);
                    } else {
                        $response = new Response("error", "No id parameter found for vehicle delete");
                        $response->statusCode = 401;
                        $rh = new VlbVehicleRestHandler();
                        $rh->EncodeResponce($response);
                    }

                    break;
            }
            break;
        case "gasload":
            if (isset($dataPayload))
                VlbExpensesRestHandler::VehicleGasLoad($dataPayload->vehicleId, $userId);
            break;
        case "gas":

            switch ($method) {
                case 'PUT':
                    VlbExpensesRestHandler::GasAdd($dataPayload->gas, $userId);
                    break;
                case 'POST':
                    VlbExpensesRestHandler::GasSave($dataPayload->gas, $userId);
                    break;
                case 'DELETE':
                    $response = new Response("error", "No id parameter found for vehicle delete");
                    $response->statusCode = 401;
                    $rh = new VlbVehicleRestHandler();
                    $rh->EncodeResponce($response);
                    break;
            }
            break;
        default:
            LoggerVlb::log($mn, "No heandler for view: " . $view);
            $response = new Response("error", "No heandler for view: " . $view);
            $response->statusCode = 401;
            $rh = new VlbVehicleRestHandler();
            $rh->EncodeResponce($response);
            break;
    }
} else {
    VlbVehicleRestHandler::AuthenticationRequired();
}
LoggerVlb::logEnd($mn);


