<?php

/*
 * -----------------------------------------------------------------------------
 *  Project             : api.account    
 *  Date Creation  : Apr 6, 2018 
 *  Filename           : VlbVehicleRestHandler.php
 *  Author              : IZIordanov
 * -----------------------------------------------------------------------------
 *  Copyright(C) 2000-2018 IZIordanov
 *  
 *  This program is free software; you can redistribute it and/or modify it under 
 *  the terms of the GNU General Public License published by the Free Software Foundation.
 * -----------------------------------------------------------------------------
 */
require_once("LoggerVlb.php");
require_once("./ConnectionVlb.php");
require_once("SimpleRest.class.php");
require_once 'Response.class.php';
require_once("JwtAuth.php");
require_once("VlbUser.class.php");
require_once("VlbVehicle.class.php");

/**
 * Description of VlbVehicleRestHandler
 *
 * @author IZIordanov
 */
class VlbVehicleRestHandler extends SimpleRest {
    
    // <editor-fold defaultstate="collapsed" desc=" JWT Tocken Methods">
    
    public static function AuthenticationRequired() {
        $mn = "VlbVehicleRestHandler::AuthenticationRequired()";
        $response = new Response("error", "Authentication Required.");
        $response->statusCode=401;
        $rh = new VlbVehicleRestHandler();
        $rh->EncodeResponce($response);
    }
    
    
    
    
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc=" Vehicle">
    
    public static function Vehicles($userId, $userName) {
        $mn = "VlbVehicleRestHandler::Vehicles('.$userId.')";
        LoggerVlb::logBegin($mn);
        $response = null;
        try {
            $vlbDataArray = VlbVehicle::LoadByUserId($userId);
            if (isset($vlbDataArray)) {
                    $response = new Response("success", $userName + " vehicles list.");
                $response->addData("vehicles",$vlbDataArray);
                $response->addData("userId",$userId);
                
                // Stat
                $sql = "SELECT st.*, g.gas_date, g.gas_quantity, g.gas_total_price, 
                    g.cal_price_per_l as gas_cal_price_per_l,
                    e.exp_date, e.exp_price, e.exp_note
                    FROM iordanov_vlb.vlb_vehicle_stat st
                    join iordanov_vlb.vlb_vehicle v on v.vehicle_id = st.vehicle_id
                    left join iordanov_vlb.vlb_expenses e on e.id = st.last_expence_id
                    left join iordanov_vlb.vlb_gas g on g.id = st.last_fillup_id
                    where v.user_id=?";
                
                $bound_params_r = array('i', $userId);
                $conn = ConnectionVlb::dbConnect();
                $logModel = LoggerVlb::currLogger()->getModule($mn);
                $ret_stat = $conn->SelectJson($sql, $bound_params_r, $logModel);
            
                $response->addData("vehicles_stat",$ret_stat);
                
           
            }
            else{
               $response = new Response("error", " vehicles list NOT found.");
            }
        } catch (Exception $ex) {
            LoggerVlb::logError($mn,  $ex);
            $response = new Response($ex);
        }
        LoggerVlb::log($mn, " response = " . $response->toJSON());
        LoggerVlb::logEnd($mn);
        $rh = new VlbVehicleRestHandler();
        $rh->EncodeResponce($response);
    }
    
    public static function Vehicle($id, $userId) {
        $mn = "VlbVehicleRestHandler::Vehicle('.$id.')";
        LoggerVlb::logBegin($mn);
        $response = null;
        try {
            $vlbVehicle = new VlbVehicle();
            $vlbVehicle->loadById($id);
            if (isset($vlbVehicle)) {
                    $response = new Response("success", " vehicles loaded.");
                $response->addData("vehicle",$vlbVehicle);
                $response->addData("userId",$userId);
                
                // Stat
                $sql = "SELECT st.*, g.gas_date, g.gas_quantity, g.gas_total_price, 
                    g.cal_price_per_l as gas_cal_price_per_l,
                    e.exp_date, e.exp_price, e.exp_note
                    FROM iordanov_vlb.vlb_vehicle_stat st
                    join iordanov_vlb.vlb_vehicle v on v.vehicle_id = st.vehicle_id
                    left join iordanov_vlb.vlb_expenses e on e.id = st.last_expence_id
                    left join iordanov_vlb.vlb_gas g on g.id = st.last_fillup_id
                    where st.vehicle_id=?";
                
                $bound_params_r = array('i', $id);
                $conn = ConnectionVlb::dbConnect();
                $logModel = LoggerVlb::currLogger()->getModule($mn);
                $ret_stat = $conn->SelectJson($sql, $bound_params_r, $logModel);
            
                $response->addData("vehicle_stat",$ret_stat);
                
           
            }
            else{
               $response = new Response("error", " vehicles list NOT found.");
            }
        } catch (Exception $ex) {
            LoggerVlb::logError($mn,  $ex);
            $response = new Response($ex);
        }
        LoggerVlb::log($mn, " response = " . $response->toJSON());
        LoggerVlb::logEnd($mn);
        $rh = new VlbVehicleRestHandler();
        $rh->EncodeResponce($response);
    }
    
    public static function VehicleSave($id, $vehicle, $userId) {
        $mn = "VlbVehicleRestHandler::VehicleSave('.$id.')";
        LoggerVlb::logBegin($mn);
        $response = null;
        try {
            $vlbVehicle = new VlbVehicle();
            $vlbVehicle->loadById($id);
            if (isset($vlbVehicle) && $vlbVehicle->getUserId() == $userId) {
                
                $vlbVehicle->setName($vehicle->name);
                $vlbVehicle->setModel($vehicle->model);
                //LoggerVlb::log($mn, " purchase_date = " . $vehicle->purchase_date->date);
                $vlbVehicle->setPurchaseDate($vehicle->purchase_date->date);
                $vlbVehicle->setFirstRegistration($vehicle->first_reg->date);
                if(isset($vehicle->sale_date) && isset($vehicle->sale_date->date))
                    $vlbVehicle->setSaleDate($vehicle->sale_date->date);
                $vlbVehicle->setMilageKmStart($vehicle->milage_km_start);
                $vlbVehicle->setMilageKmCurrent($vehicle->milage_km_current);
                $vlbVehicle->setTrackInMi($vehicle->track_in_mi);
                
                $updatedVehicle = $vlbVehicle->save();
                
                $response = new Response("success", " vehicles updated.");
                $response->addData("vehicle",$updatedVehicle);
                $response->addData("userId",$userId);
            }
            else{
               $response = new Response("error", " vehicles NOT found.");
            }
        } catch (Exception $ex) {
            LoggerVlb::logError($mn,  $ex);
            $response = new Response($ex);
        }
        LoggerVlb::log($mn, " response = " . $response->toJSON());
        LoggerVlb::logEnd($mn);
        $rh = new VlbVehicleRestHandler();
        $rh->EncodeResponce($response);
    }
    
    public static function VehicleDelete($id, $userId) {
        $mn = "VlbVehicleRestHandler::VehicleDelete('.$id.')";
        LoggerVlb::logBegin($mn);
        $response = null;
        try {
            
                VlbVehicle::DeleteById($id);
                $response = new Response("success", " vehicles deleted.");
            
        } catch (Exception $ex) {
            LoggerVlb::logError($mn,  $ex);
            $response = new Response($ex);
        }
        LoggerVlb::log($mn, " response = " . $response->toJSON());
        LoggerVlb::logEnd($mn);
        $rh = new VlbVehicleRestHandler();
        $rh->EncodeResponce($response);
    }
    
    public static function VehicleNew($vehicle, $userId) {
        $mn = "VlbVehicleRestHandler::VehicleNew('.$userId.')";
        LoggerVlb::logBegin($mn);
        $response = null;
        try {
            $vlbVehicle = VlbVehicle::Create($userId, $vehicle->model, $vehicle->milage_km_start);
            $vlbVehicle->loadById($id);
            if (isset($vlbVehicle) && $vlbVehicle->getUserId() == $userId) {
                
                $vlbVehicle->setName($vehicle->name);
                $vlbVehicle->setModel($vehicle->model);
                //LoggerVlb::log($mn, " purchase_date = " . $vehicle->purchase_date->date);
                $vlbVehicle->setPurchaseDate($vehicle->purchase_date->date);
                $vlbVehicle->setFirstRegistration($vehicle->first_reg->date);
                if(isset($vehicle->sale_date) && isset($vehicle->sale_date->date))
                    $vlbVehicle->setSaleDate($vehicle->sale_date->date);
                $vlbVehicle->setMilageKmStart($vehicle->milage_km_start);
                $vlbVehicle->setMilageKmCurrent($vehicle->milage_km_current);
                $vlbVehicle->setTrackInMi($vehicle->track_in_mi);
                
                $updatedVehicle = $vlbVehicle->save();
                
                $response = new Response("success", " vehicles created.");
                $response->addData("vehicle",$updatedVehicle);
                $response->addData("userId",$userId);
            }
            else{
               $response = new Response("error", " vehicles NOT found.");
            }
        } catch (Exception $ex) {
            LoggerVlb::logError($mn,  $ex);
            $response = new Response($ex);
        }
        LoggerVlb::log($mn, " response = " . $response->toJSON());
        LoggerVlb::logEnd($mn);
        $rh = new VlbVehicleRestHandler();
        $rh->EncodeResponce($response);
    }
    
    // </editor-fold>
}
